package hello;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public @ResponseBody String provideUploadInfo() {
		return "You can upload a file by posting to this same URL.";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String handleFileUpload(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file, HttpServletRequest req,
			HttpServletResponse response) {
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				File f = new File(name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
				System.out.println("File written to "+f.getAbsolutePath());
				stream.write(bytes);
				stream.close();
		       response.setHeader("Location", "download?" + name);
				return "File uploaded" + name + "!";

				
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name + " because the file was empty.";
		}
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public @ResponseBody String handleFileDownload(@RequestParam("name") String name, HttpServletRequest req,
			HttpServletResponse response) throws Exception {
		OutputStream out = response.getOutputStream();
		FileInputStream in = new FileInputStream(name);
		byte[] buffer = new byte[4096];
		int length;
		while ((length = in.read(buffer)) > 0) {
			out.write(buffer, 0, length);
		}
		in.close();
		out.flush();
		return "";

	}
}
